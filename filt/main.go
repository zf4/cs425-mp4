package main

import (
	"strconv"
)

// Process - process
func Process(tuple []interface{}) []interface{} {
	s := tuple[0].(string)
	val, _ := strconv.Atoi(s)
	retval := make([]interface{}, 0)
	if val > 50 {
		retval = append(retval, val)
	}
	return retval
}

func main() {

}
