package main

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

var V map[int]string

// Process - process
func Process(tuple []interface{}) []interface{} {
	if V == nil {
		V = make(map[int]string)
	}
	if _, ok := V[1]; !ok {
		// Open data base to determine V[1] and V[2]
		file, err := os.Open("../dataset/joindatabase.txt")
		if err != nil {
			panic(err)
		}
		defer file.Close()
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			s := scanner.Text()
			str := strings.Split(s, ":")
			i, _ := strconv.Atoi(str[0])
			sex := str[1]
			V[i] = sex
		}
	}
	s := tuple[0].(string)
	val, _ := strconv.Atoi(s)
	retval := []interface{}{V[val]}
	return retval
}

func main() {

}
