package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"

	"./crane"
	"./failure"
	"./file_sys"
	"./grep_server"
	"./shared"
)

const CMD_PROMPT = "> "

var craneType int

func main() {
	println("Starting server")
	log.SetFlags(log.Lshortfile)

	failure.Initialize()

	go func() {
		println("Type 'help' for list of commands")
		fmt.Printf("[Server %d]%s", shared.GetOwnServerNumber(), CMD_PROMPT)
		for {
			reader := bufio.NewReader(os.Stdin)
			cmd, _ := reader.ReadString('\n')
			go func() {
				HandleServerCommand(strings.TrimSuffix(cmd, "\n"))
				fmt.Printf("[Server %d]%s", shared.GetOwnServerNumber(), CMD_PROMPT)
			}()
		}
	}()

	file_sys.Initialize()

	// Setup as server, client, or worker
	if len(os.Args) > 1 && os.Args[1] == "crane-client" {
		// Usage: ./cs425 [crane-client]
		craneType = 1
		shared.CraneClientServNum = shared.GetOwnServerNumber()
		go crane.ConnectCraneMaster()
	} else if shared.GetOwnServerNumber() == 2 || shared.GetOwnServerNumber() == 3 {
		craneType = 2
		go crane.StartMaster()
	} else {
		craneType = 3
		go crane.StartWorking()
	}

	grep_server.Initialize()

	println("Finished starting server")
	// Keep the program running so it doesn't close the port
	for {
	}
}

func HandleServerCommand(cmd string) {
	switch com := strings.Split(cmd, " "); com[0] {
	case "":
		{
			println()
		}
	case "leave":
		{
			fmt.Println("Leaving group")
			failure.LeaveGroup()
			file_sys.Leave()
		}
	case "print_fail":
		{
			shared.PrintFailDetectInfo = !shared.PrintFailDetectInfo
		}
	case "memlist":
		{
			println(failure.MemList.Str(true))
		}
	case "clear":
		{
			cmd := exec.Command("clear")
			cmd.Stdout = os.Stdout
			cmd.Run()
		}
	case "put", "get", "delete", "ls", "store", "get-versions", "test":
		{
			fileCmdError := file_sys.HandleFileCmd(com[0], com[1:])
			if fileCmdError != nil {
				fmt.Printf("%v\n", fileCmdError)
			}
			println()
		}
	case "help":
		{
			fmt.Printf("leave\nprint_fail\nmem_list\nput\nget\ndelete\nls\nstore\nget-versions\n\n")
		}
	case "bolt":
		{
			// Usage: bolt [pluginName-functionName] [boltName] [ancestorName1 ancestorName2 ...
			if craneType == 1 {
				crane.ProcessCraneTopology(com[1:])
			} else {
				fmt.Println("This is not a crane client")
			}
		}
	case "submit":
		{
			// Usage: submit [jobSource] [outputFile]
			if craneType == 1 {
				crane.ProcessCraneJob(com[1:])
			} else {
				fmt.Println("This is not a crane client")
			}
		}
	case "dump-crane":
		{
			if craneType == 2 {
				crane.SaveToFile()
			} else {
				fmt.Println("This is not a master")
			}
		}
	case "print-temp":
		{
			if craneType == 2 {
				crane.PrintTempData()
			} else {
				fmt.Println("This is not a master")
			}
		}
	default:
		println("Invalid Command")
	}
}
