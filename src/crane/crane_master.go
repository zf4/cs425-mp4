package crane

import (
	"bufio"
	"bytes"
	"encoding/gob"
	"fmt"
	"io"
	"math/rand"
	"net"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"../failure"
	"../file_sys"
	"../shared"
)

// BoltInfo - contains all information about a bolt on a worker
type BoltInfo struct {
	Self      Bolt
	Ancestors map[int]Bolt
	Children  map[int]Bolt
}

var boltList []BoltInfo

var serverStatus [11]bool // true - normal; false - failed
var activeList []int
var isWorking bool              // true if crane is currently work on some jobs
var jobFinished bool            // true if a job is just finished
var boltID int                  // unique id for each bolt
var possibleServerIdx int       // index of last chosen server
var workerConn map[int]net.Conn // map worker servNum to its corresponding connection
var workerDataConn map[int]net.Conn
var workerPort int
var needRestart bool
var tempData []interface{}
var currJob int

var activeListMux sync.Mutex
var dataSaveMux sync.Mutex

// ---- The following parts are about Spout functionalities ----
// info of children of this spout
var childrenBolts []Bolt

// input source
var spoutInput string

// Output
var craneOutput string
var fd *os.File

var debugCount int

// ---- end spout var definitions ----

// StartMaster - Master starts here
func StartMaster() {
	currJob = 0
	InitializeCraneMaster()
	go CheckMasterStatus()
	go ListenForClient()
	go ListenForDataCollection()
	fmt.Println("Crane master started.")
}

// InitializeCraneMaster - initialize the master
func InitializeCraneMaster() {
	debugCount = 0
	fd = nil
	needRestart = false
	isWorking = false
	jobFinished = false
	boltID = 1 // 0 for spout, i.e., master node
	possibleServerIdx = 0
	workerConn = make(map[int]net.Conn)
	workerDataConn = make(map[int]net.Conn)

	childrenBolts = nil
	// Check if I am backup
	if shared.GetOwnServerNumber() == MASTER_ONE {
		workerPort = shared.CraneServerWorkerPort
	} else {
		workerPort = shared.CraneServerBackupPort
	}
	for i := 0; i < 11; i++ {
		serverStatus[i] = false
	}
	updateServerStatus()
}

// Check and update server status, return all failed server
func updateServerStatus() bool {
	// var failedServer []int
	flag := false
	activeListMux.Lock()
	// for i := range activeList {
	// 	for j := range failure.MemList.Servers {
	// 		if activeList[i] == int(failure.MemList.Servers[j].Id.ServNum) && failure.MemList.Servers[j].Id.Failed {
	// 			fmt.Println("Failure detected!")
	// 			flag = true
	// 			break
	// 		}
	// 	}
	// }
	newList := make([]int, 0, 5)
	for i := range failure.MemList.Servers {
		if failure.MemList.Servers[i].Id.Failed == false {
			newList = append(newList, int(failure.MemList.Servers[i].Id.ServNum))
		}
	}
	if len(newList) < len(activeList) {
		flag = true
	}
	activeList = newList
	//fmt.Println(activeList)
	activeListMux.Unlock()
	return flag
}

// CheckMasterStatus - check crane status to see if a reinitialization or restart is needed
func CheckMasterStatus() {
	for {
		//fmt.Println("Checking status...")
		failedServer := updateServerStatus()
		// One of the worker failed
		if failedServer && isWorking {
			fmt.Println("A worker failed")
			needRestart = true
			startNewJob()
		}
		if jobFinished {
			InitializeCraneMaster()
		}
		time.Sleep(shared.CraneTimeoutInterval)
	}
}

// ListenForClient - Open port for clients
func ListenForClient() {
	port := strconv.Itoa(shared.CraneServerCilentPort)
	l, err := net.Listen("tcp", ":"+port)
	if err != nil {
		fmt.Println(err)
	}
	defer l.Close()
	for {
		c, err := l.Accept()
		if err != nil {
			fmt.Println(err)
		}
		go handleClientConnection(c)
	}
}

// ListenForDataCollection - Open port for data collection
func ListenForDataCollection() {
	port := strconv.Itoa(shared.CraneDataCollectionPort)
	l, err := net.Listen("tcp", ":"+port)
	if err != nil {
		fmt.Println(err)
	}
	defer l.Close()
	for {
		c, err := l.Accept()
		if err != nil {
			fmt.Println(err)
		}
		go handleWorkerConnection(c)
	}
}

func handleClientConnection(conn net.Conn) {
	for {
		buffer := make([]byte, 1024)
		n, _ := conn.Read(buffer)
		fmt.Println("New client request received!")
		s := string(buffer[:n])
		args := strings.Fields(s)
		if args[0] == "bolt" {
			fmt.Println("Adding the new topology...")
			addNewBolt(args)
		} else if args[0] == "submit" {
			fmt.Println("Submitting the new job..")
			spoutInput = args[1]
			craneOutput = args[2]
			// fd, err = os.Create(craneOutput)
			// if err != nil {
			// 	panic(err)
			// }
			startNewJob()
			isWorking = true
			fmt.Println(isWorking)
		}
	}
	conn.Close()
}

// Add a new bolt to boltList. Note that servers are not assigned until the job is submitted.
func addNewBolt(args []string) {
	// Extract information from args
	pluginName := args[1]
	boltName := args[2]
	ancestorNameGroup := args[3:]
	// Build a new "empty" bolt
	ancestors := make(map[int]Bolt)
	children := make(map[int]Bolt)
	bolt := Bolt{
		ID:         boltID,
		Name:       boltName,
		ServNum:    0,
		PluginName: pluginName,
	}
	// Update boltID so that each new bolt has a unique id
	boltID++
	// Connect the new bolt with existing bolts
	for _, ancestorName := range ancestorNameGroup {
		if ancestorName == "spout" {
			ancestors[0] = Bolt{
				ID:         0,
				Name:       "spout",
				ServNum:    shared.GetOwnServerNumber(),
				PluginName: "dummy"}
			childrenBolts = append(childrenBolts, bolt)
		} else {
			flag := true
			for i, bInfo := range boltList {
				if bInfo.Self.Name == ancestorName {
					ancestors[bInfo.Self.ID] = Bolt{
						ID:         bInfo.Self.ID,
						Name:       bInfo.Self.Name,
						ServNum:    bInfo.Self.ServNum,
						PluginName: bInfo.Self.PluginName}
					boltList[i].Children[bolt.ID] = bolt
					flag = false
				}
			}
			if flag {
				fmt.Println("No such ancestor exists. Please try to insert its ancestor bolt first.")
			}
		}
	}
	// Use the new bolt to construct a BoltInfo
	boltInfo := BoltInfo{Self: bolt, Ancestors: ancestors, Children: children}
	boltList = append(boltList, boltInfo)
}

func startNewJob() {
	// Assign bolts to servers
	for i := range boltList {
		// Set worker for current bolt
		boltList[i].Self.ServNum = pickServerNum(boltList[i])
		// Update current bolt's children and ancestors' ServNum to connect them
		updateBoltAncestorChildren(boltList[i])
	}
	// Send bolts to their corresponding workers
	//var wg sync.WaitGroup
	for _, elem := range boltList {
		currWorker := elem.Self.ServNum
		//wg.Add(1)
		//go func(worker int, boltInfo BoltInfo) {
		//defer wg.Done()
		// Set up a new connection if there is no conn for this worker
		if _, ok := workerConn[currWorker]; !ok {
			fmt.Printf("Connecting to worker: %v\n", currWorker)
			conn, err := net.Dial("tcp", shared.GetServerAddressFromNumber(currWorker)+":"+strconv.Itoa(workerPort))
			if err != nil {
				panic(err)
			}
			workerConn[currWorker] = conn
		}
		// Send bolt info using gob encoder
		fmt.Println("Sending topology...")
		fmt.Println(elem)
		buf := new(bytes.Buffer)
		gobEncoder := gob.NewEncoder(buf)
		gobEncoder.Encode(elem)
		workerConn[currWorker].Write(buf.Bytes())
		// read the response
		// readBuf := make([]byte, 1024)
		// n, err := workerConn[currWorker].Read(readBuf)
		// if err != nil {
		// 	fmt.Println(err)
		// }
		//fmt.Println(string(readBuf[:n]))
		//}(currWorker, elem)
	}
	// Update my children list
	for i, b := range childrenBolts {
		for _, bInfo := range boltList {
			if b.ID == bInfo.Self.ID {
				childrenBolts[i].ServNum = bInfo.Self.ServNum
				break
			}
		}
	}
	tempData = make([]interface{}, 0)
	currJob++
	//wg.Wait()
	// start spout goroutine
	go runSpout(currJob)
}

// pick an available server number (worker) for the bolt
func pickServerNum(boltInfo BoltInfo) int {
	for {
		flag := true
		activeListMux.Lock()
		if possibleServerIdx >= len(activeList) {
			possibleServerIdx = 0
		}
		//fmt.Println(activeList)
		possibleServer := activeList[possibleServerIdx]
		activeListMux.Unlock()
		// Skip masters -- masters cannot be workers
		if possibleServer == MASTER_ONE || possibleServer == MASTER_BACKUP || possibleServer == 4 {
			possibleServerIdx++
			flag = false
		}
		// Check if the bolt and its children/ancestors are assigned to the same worker. If so, find another worker.
		for _, ancestor := range boltInfo.Ancestors {
			if possibleServer == ancestor.ServNum {
				possibleServerIdx++
				flag = false
				break
			}
		}
		for _, child := range boltInfo.Children {
			if possibleServer == child.ServNum {
				possibleServerIdx++
				flag = false
				break
			}
		}
		if flag {
			possibleServerIdx++
			return possibleServer
		}
	}
	// If everything works, this line should not be reached
	return -1
}

// A very ugly function that to let boltInfo's connections know its ServNum
func updateBoltAncestorChildren(boltInfo BoltInfo) {
	// Update ancestors
	for i := range boltList {
		if val, ok := boltList[i].Children[boltInfo.Self.ID]; ok {
			boltList[i].Children[boltInfo.Self.ID] = Bolt{
				ID:         val.ID,
				Name:       val.Name,
				ServNum:    boltInfo.Self.ServNum,
				PluginName: val.PluginName}
		}
	}
	// Update children
	for i := range boltList {
		if val, ok := boltList[i].Ancestors[boltInfo.Self.ID]; ok {
			boltList[i].Ancestors[boltInfo.Self.ID] = Bolt{
				ID:         val.ID,
				Name:       val.Name,
				ServNum:    boltInfo.Self.ServNum,
				PluginName: val.PluginName}
		}
	}
}

func runSpout(myjobid int) {
	// If I am backup and main master is still alive, I should not run spout
	if !mainMasterFailed() && shared.GetOwnServerNumber() == MASTER_BACKUP {
		return
	}
	file, err := os.Open("../dataset/" + spoutInput)
	if err != nil {
		panic(err)
	}
	defer file.Close()
	fmt.Println("New job started running...")
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		time.Sleep(shared.CraneSpoutInterval)
		// activeListMux.Lock()
		// fmt.Println(activeList)
		// activeListMux.Unlock()
		tuple := []interface{}{scanner.Text()}
		// pick up a child and send it the data
		// at random maybe ...?
		child := childrenBolts[rand.Intn(len(childrenBolts))]
		data := Data{ID: child.ID, Tuple: tuple, JobId: myjobid}
		childServ := child.ServNum
		// fmt.Printf("Send data to bolt %v\n", child.ID)
		// fmt.Println(data)
		// fmt.Println("Print children")
		// fmt.Println(childrenBolts)
		// fmt.Println(child)
		// Send data
		if _, ok := workerDataConn[childServ]; !ok {
			conn, err := net.Dial("tcp", shared.GetServerAddressFromNumber(childServ)+":"+strconv.Itoa(shared.CraneWorkerPort))
			if err != nil {
				panic(err)
			}
			workerDataConn[childServ] = conn
		}

		buf := new(bytes.Buffer)
		gobEncoder := gob.NewEncoder(buf)
		gobEncoder.Encode(data)
		workerDataConn[childServ].Write(buf.Bytes())
		// read the response
		// readBuf := make([]byte, 1024)
		// n, err := conn.Read(readBuf)
		// if err != nil {
		// 	fmt.Println(err)
		// }
		// fmt.Println(string(readBuf[:n]))
		//conn.Close()
		if needRestart {
			needRestart = false
			return
		}
	}
	fmt.Println("Finish reading all tuples")
	if err := scanner.Err(); err != nil {
		fmt.Println(err)
	}
}

func handleWorkerConnection(conn net.Conn) {
	for {
		buf := make([]byte, 1024)
		_, err := conn.Read(buf)
		if err != nil && err != io.EOF {
			fmt.Println(err)
		}
		if err == nil {
			// Decode the data received
			tempBuffer := bytes.NewBuffer(buf)
			data := new(Data)
			gobDecoder := gob.NewDecoder(tempBuffer)
			gobDecoder.Decode(data)
			// Write ack response
			//conn.Write([]byte("Ack"))

			// TODO: Process data
			// fmt.Println(*data)
			debugCount++
			if debugCount%10000 == 0 {
				fmt.Println(debugCount)
			}
			if data.JobId == currJob {
				tempNewLine := []interface{}{"\n"}
				dataSaveMux.Lock()
				tempData = append(tempData, data.Tuple...)
				tempData = append(tempData, tempNewLine...)
				dataSaveMux.Unlock()
			}

			//saveData(data.Tuple)
		}
	}
	conn.Close()
}

func appendDataToFile(tuple []interface{}) {
	dataSaveMux.Lock()
	f, err := os.OpenFile(craneOutput, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	for _, elem := range tuple {
		tmpStr := fmt.Sprintf("%v", elem)
		if _, err = f.WriteString(tmpStr); err != nil {
			fmt.Println(err)
		}
	}
	dataSaveMux.Unlock()
	return
}

func saveTempData() {
	dataSaveMux.Lock()
	f, err := os.Create(craneOutput)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	for _, elem := range tempData {
		tmpStr := fmt.Sprintf("%v", elem)
		if _, err = f.WriteString(tmpStr); err != nil {
			fmt.Println(err)
		}
	}
	dataSaveMux.Unlock()
	return
}

// Save current output to a file in sdfs
func SaveToFile() {
	saveTempData()
	dataSaveMux.Lock()
	tmp := make([]string, 0)
	tmp = append(tmp, craneOutput)
	tmp = append(tmp, "craneOut")
	file_sys.HandleFileCmd("put", tmp)
	dataSaveMux.Unlock()
}

func PrintTempData() {
	dataSaveMux.Lock()
	for _, elem := range tempData {
		tmpStr := fmt.Sprintf("%v", elem)
		fmt.Println(tmpStr)
	}
	dataSaveMux.Unlock()
}

// ServerCleanup - shut down all conn to workers
func ServerCleanup() {
	for _, conn := range workerConn {
		conn.Close()
	}
}
