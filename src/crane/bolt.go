package crane

/**
	This file contains the definition of Bolt class and defines the API of topology definition
**/

// Bolt - A single bolt
type Bolt struct {
	ID         int    // The unique id of this bolt
	Name       string // The name of this bolt
	ServNum    int    // Server number of where current bolt is deployed
	PluginName string // plugin ran on this bolt
}
