package crane

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"io"
	"net"
	"plugin"
	"strconv"
	"strings"
	"time"

	"../failure"
	"../shared"
)

//var currListen net.Listener        // current master that worker is listening to (master or backup)
var servNumWorker map[int]net.Conn // map servNum to worker Conn
var boltMap map[int]BoltInfo       // map boltID to corresponding BoltInfo
var pluginMap map[string]*plugin.Plugin

// Data - definition of data transferred between workers
type Data struct {
	ID    int
	Tuple []interface{}
	JobId int
}

func listenToServer(port int) net.Listener {
	ln, err := net.Listen("tcp", ":"+strconv.Itoa(port))
	if err != nil {
		panic(err)
	}
	return ln
}

// StartWorking - worker starts here
func StartWorking() {
	servNumWorker = make(map[int]net.Conn)
	boltMap = make(map[int]BoltInfo)
	pluginMap = make(map[string]*plugin.Plugin)
	// listen to worker communication port first
	workerListen := listenToServer(shared.CraneWorkerPort)
	// listen to masters
	masterListen := listenToServer(shared.CraneServerWorkerPort)
	backupListen := listenToServer(shared.CraneServerBackupPort)
	// defer workerListen.Close()
	// defer masterListen.Close()
	// defer backupListen.Close()
	//currListen = masterListen

	// accept master's bolt assignment info
	go listenForRequest(masterListen, handleMasterRequest)
	go listenForRequest(backupListen, handleMasterRequest)
	// accept worker's tuples
	go listenForRequest(workerListen, handleWorkerRequest)

	fmt.Println("Crane worker started.")
	// Keep the program running so it doesn't close the port
	// for {
	// }
}

func listenForRequest(currListen net.Listener, handler func(net.Conn)) {
	for {
		// Accept message
		conn, err := currListen.Accept()
		if err != nil {
			fmt.Println(err)
		}
		go handler(conn)
	}
}

func handleMasterRequest(conn net.Conn) {
	for {
		buf := make([]byte, 1024)
		_, err := conn.Read(buf)
		if err != nil && err != io.EOF {
			fmt.Println(err)
		}
		if err == nil {
			fmt.Println("Master request received!")
			tempBuffer := bytes.NewBuffer(buf)
			boltInfo := new(BoltInfo)
			gobDecoder := gob.NewDecoder(tempBuffer)
			gobDecoder.Decode(boltInfo)
			// boltInfo now decoded to a BoltInfo
			boltMap[boltInfo.Self.ID] = *boltInfo
			// write an ack back to let master know the worker has received the message
			//conn.Write([]byte("Bolt received"))
			// fmt.Println(*boltInfo)
		}
		time.Sleep(100 * time.Millisecond)
	}
	conn.Close()
}

func handleWorkerRequest(conn net.Conn) {
	for {
		buf := make([]byte, 1024)
		_, err := conn.Read(buf)
		if err != nil && err != io.EOF {
			fmt.Println(err)
		}
		if err == nil {
			// Decode the data received
			tempBuffer := bytes.NewBuffer(buf)
			data := new(Data)
			gobDecoder := gob.NewDecoder(tempBuffer)
			gobDecoder.Decode(data)
			// Write ack response
			//conn.Write([]byte("Ack"))
			processData(*data)
		}
	}
	conn.Close()
}

func processData(data Data) {
	// Get the bolt who is responsible for processing data
	bolt := boltMap[data.ID]
	// fmt.Print(bolt)
	// Call the plugin to process data
	s := strings.Split(bolt.Self.PluginName, "-")
	// fmt.Println(bolt.Self.PluginName)
	// fmt.Println(s)
	pluginName := ("../plugins/" + s[0] + ".so")
	functionName := s[1]
	if _, ok := pluginMap[pluginName]; !ok {
		pluginMap[pluginName] = loadPlugin(pluginName)
	}
	f, err := pluginMap[pluginName].Lookup(functionName)
	if err != nil {
		panic(err)
	}
	boltFunc, ok := f.(func([]interface{}) []interface{})
	if !ok {
		panic("No such plugin or function exist")
	}
	//fmt.Println(data.Tuple)
	processedTuple := boltFunc(data.Tuple)
	fmt.Println(processedTuple)
	newData := Data{ID: 0, Tuple: processedTuple, JobId: data.JobId}
	// Send the processed data
	if len(processedTuple) > 0 {
		sendProcessedData(newData, bolt)
	}
}

func loadPlugin(pluginName string) *plugin.Plugin {
	//fmt.Println(pluginName)
	p, err := plugin.Open(pluginName)
	if err != nil {
		panic(err)
	}
	return p
}

func sendProcessedData(newdata Data, bolt BoltInfo) {
	// If this bolt has no children, send it back to master for data collection
	if len(bolt.Children) == 0 {
		var masterServNum int
		if mainMasterFailed() {
			masterServNum = MASTER_BACKUP
		} else {
			masterServNum = MASTER_ONE
		}
		//fmt.Printf("Data send to %v\n", masterServNum)
		data := Data{ID: 0, Tuple: newdata.Tuple, JobId: newdata.JobId}
		port := strconv.Itoa(shared.CraneDataCollectionPort)
		sendDataToWorker(data, masterServNum, port)
	} else {
		for _, child := range bolt.Children {
			// Construct data to be sent
			data := Data{ID: child.ID, Tuple: newdata.Tuple, JobId: newdata.JobId}
			// Find correct servNum
			servNum := child.ServNum
			// Send data
			port := strconv.Itoa(shared.CraneWorkerPort)
			sendDataToWorker(data, servNum, port)
		}
	}
}

func sendDataToWorker(data Data, servNum int, port string) {
	// Established connection if haven't done that yet
	if _, ok := servNumWorker[servNum]; !ok {
		conn, err := net.Dial("tcp", shared.GetServerAddressFromNumber(servNum)+":"+port)
		if err != nil {
			panic(err)
		}
		servNumWorker[servNum] = conn
	}
	// Send data using gob encoder
	buf := new(bytes.Buffer)
	gobEncoder := gob.NewEncoder(buf)
	gobEncoder.Encode(data)
	servNumWorker[servNum].Write(buf.Bytes())
	// read the response
	// readBuf := make([]byte, 1024)
	// n, err := servNumWorker[servNum].Read(readBuf)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	//fmt.Println(string(readBuf[:n]))
}

func sendDataBackToMaster(data Data, servNum int) {
	conn, err := net.Dial("tcp", shared.GetServerAddressFromNumber(servNum)+":"+strconv.Itoa(shared.CraneDataCollectionPort))
	if err != nil {
		panic(err)
	}
	buf := new(bytes.Buffer)
	gobEncoder := gob.NewEncoder(buf)
	gobEncoder.Encode(data)
	conn.Write(buf.Bytes())
	// read the response
	// readBuf := make([]byte, 1024)
	// n, err := conn.Read(readBuf)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	//fmt.Println(string(readBuf[:n]))
}

// Check if main master has failed
func mainMasterFailed() bool {
	for i := range failure.MemList.Servers {
		if int(failure.MemList.Servers[i].Id.ServNum) == MASTER_ONE && failure.MemList.Servers[i].Id.Failed == true {
			return true
		}
	}
	return false
}
