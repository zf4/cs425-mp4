package crane

import (
	"fmt"
	"net"
	"strconv"

	"../failure"
	"../shared"
)

// The masters' server num
const MASTER_ONE = 2
const MASTER_BACKUP = 3

var masterConn net.Conn
var masterBackupConn net.Conn

// ProcessCraneTopology - build up the topology of crane
func ProcessCraneTopology(args []string) {
	if len(args) != 3 {
		fmt.Println("Invalid number of arguments. Usage: bolt [ancestor] [pluginName] [boltName]")
		return
	}
	pluginName := args[0]
	currName := args[1]
	ancestorName := args[2:]
	// Construct information to be delivered
	info := ("bolt " + pluginName + " " + currName + " ")
	for _, ancName := range ancestorName {
		info += ancName
		info += " "
	}
	for i := range failure.MemList.Servers {
		if int(failure.MemList.Servers[i].Id.ServNum) == MASTER_ONE && !failure.MemList.Servers[i].Id.Failed {
			fmt.Println("Send topology to master...")
			masterConn.Write([]byte(info))
		}
		if int(failure.MemList.Servers[i].Id.ServNum) == MASTER_BACKUP && !failure.MemList.Servers[i].Id.Failed {
			fmt.Println("Send topology to backup...")
			masterBackupConn.Write([]byte(info))
		}
	}
}

// ProcessCraneJob - submit job to the built crane
func ProcessCraneJob(args []string) {
	if len(args) != 2 {
		fmt.Println("Invalid number of arguments. Usage: submit [inputFileName] [outputFileName]")
		return
	}
	jobSource := args[0]
	outputFile := args[1]
	for i := range failure.MemList.Servers {
		if int(failure.MemList.Servers[i].Id.ServNum) == MASTER_ONE && !failure.MemList.Servers[i].Id.Failed {
			fmt.Println("Send job to master...")
			masterConn.Write([]byte("submit " + jobSource + " " + outputFile))
		}
		if int(failure.MemList.Servers[i].Id.ServNum) == MASTER_BACKUP && !failure.MemList.Servers[i].Id.Failed {
			fmt.Println("Send job to backup...")
			masterBackupConn.Write([]byte("submit " + jobSource + " " + outputFile))
		}
	}
}

// Start two tcp connections to crane master and master, Call this function in main.go
func ConnectCraneMaster() {
	masterAddr := shared.GetServerAddressFromNumber(MASTER_ONE)
	masterBackupAddr := shared.GetServerAddressFromNumber(MASTER_BACKUP)
	port := strconv.Itoa(shared.CraneServerCilentPort)
	masterConn = connectToServer(masterAddr, port)
	masterBackupConn = connectToServer(masterBackupAddr, port)
	fmt.Println("Crane client started.")
}

func connectToServer(addr string, port string) net.Conn {
	conn, err := net.Dial("tcp", addr+":"+port)
	if err != nil {
		fmt.Println(err)
	}
	return conn
}

// Cleanup - Close all connections manually
func ClientCleanup() {
	masterConn.Close()
	masterBackupConn.Close()
}
