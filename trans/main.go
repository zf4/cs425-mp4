package main

import (
	"strconv"
)

// Process - process
func Process(tuple []interface{}) []interface{} {
	s := tuple[0].(string)
	val, _ := strconv.Atoi(s)
	retval := []interface{}{val * 2}
	return retval
}

func main() {

}
