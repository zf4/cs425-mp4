package main

import (
	"strings"
)

var V map[string]int

// F - test
func ProcessInput(tuple []interface{}) []interface{} {
	s := tuple[0]
	str := strings.Fields(s.(string))
	retval := []interface{}{str[1], 1}
	return retval
}

func Count(tuple []interface{}) []interface{} {
	if V == nil {
		V = make(map[string]int)
	}
	s := tuple[0].(string)
	if _, ok := V[s]; ok {
		V[s]++
	} else {
		V[s] = 1
	}
	retval := []interface{}{s, V[s]}
	return retval
}

func main() {

}
