from __future__ import print_function

import sys

from pyspark import SparkContext
from pyspark.streaming import StreamingContext


if __name__ == "__main__":

    sc = SparkContext(appName='app1')
    sc.setLogLevel('WARN')
    ssc = StreamingContext(sc, 1)
    lines = ssc.socketTextStream(sys.argv[1], int(sys.argv[2]))

    result = lines.map(lambda num : int(num) * 2)
    
    result.pprint()
    result.saveAsTextFiles('output.txt')
    
    ssc.start()
    ssc.awaitTermination()
